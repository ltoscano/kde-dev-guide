===========
First steps
===========

.. toctree::
   :maxdepth: 2

   projects
   qt
   git-kde
   ide
   kde-apis
   documentation
