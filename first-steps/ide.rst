Choosing an IDE
===============

An integrated development environment (IDE) allows you to do project
management, testing, and other activities in a convenient way alongside
your coding. We recommend that you install one of the following IDEs and
do your KDE development work within it.

We recommend QtCreator for its ease of use and features, especially its
built-in text editor. But it's nice to know, if you're familiar with
Eclipse already, that you can use that for KDE development too.

QtCreator
---------

.. image:: /images/qtcreator-screenshots.png

QtCreator is an integrated, cross-platform IDE for C++ and JavaScript
that is part of the Qt SDK. It includes a visual debugger and a designer
tool for GUI layout and forms. The editor's features include syntax
highlighting and auto-completion. QtCreator uses the GNU C++ compiler
and related tools. On Windows it can use MinGW or MSVC with the default
install, and cdb when compiled from source.

You can find out more about using QtCreator with the KDE development
platform at:

   http://techbase.kde.org/Development/Tutorials/Using_Qt_Creator


KDevelop
--------

.. image:: /images/Kdevelop_cpp_codetooltip.png

KDevelop is a free, open source IDE (Integrated Development Environment)
created as part of the KDE development project to support development on
C/C++ and other languages on Microsoft Windows, Mac OS X, Linux, Solaris
and FreeBSD. It is feature-full and can be extended through plugins. It
is based on KDevPlatform and the KDE development platform and Qt
libraries and has been under development since 1998.

You can find out more about KDevelop at:

   http://userbase.kde.org/KDevelop4/Manual

Eclipse
-------

Eclipse is a popular open source IDE. It is used primarily for
programming in Java, but supports a number of other languages, including
C++, with the appropriate plug-in modules.

You can find out more about using Eclipse with the KDE development
platform at:

   http://techbase.kde.org/Development/Tools/Eclipse
