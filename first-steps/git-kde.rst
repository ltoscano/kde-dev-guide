Using Git for KDE Development
=============================

Git is a free and open source version control system designed to handle
everything from small to very large projects with speed and efficiency.
It provides lots of tools for figuring out where you have gone as you
edit files, as well as merging your changes with those made by other
developers.

You can find more about git (and download it if necessary) at
http://git-scm.com.

Git basics
----------

There are several levels at which your changes can reside in git. You
need to go through all the steps carefully in order to save changes
correctly. A typical sequence of git activities starts with cloning a
remote repository. Now you have a complete copy of what the original
developer has. Then you should do the following:

#. Create and edit files.
#. Run git add to tell git about any new files you have created or files
   that you have just edited. Files with changes are in an intermediate
   state called a staging area, but not in any repository yet.
#. Run git commit to save your changes in your own local repository.

We'll show examples of all this later in the chapter.

Instead of having to remember and type in the different full git
addresses for pulling and pushing, we recommend you manually add the
following to your Git User Configuration (*~/.gitconfig*):

::

   [url "git://anongit.kde.org/"]
       insteadOf = kde:
   [url "git@git.kde.org:"]
       pushInsteadOf = kde:

Cloning repositories
--------------------

After setting up your *~/.gitconfig* as shown in the previous section,
you need to clone the source repositories using the following command:

.. code-block:: console

   git clone kde:project_name

where *project_name* is the name of the project that you want to
contribute to. For instance, to submit a patch to kdelibs, start with
the command:

.. code-block:: console

   git clone kde:kdelibs

Pulling changes
---------------

If you already have a git repository that you need to update with new
changes from the original source, run the following:

.. code-block:: console

   git pull

Working with branches
---------------------

Git is a very powerful revision control system that supports the concept
of branches. In order to develop a new feature for a KDE Development
Platform Project, it's best to use a separate branch for feature
development. You can check out a new branch using the following command:

.. code-block:: console

   git checkout -b feature_name

for example:

.. code-block:: console

   git checkout -b myFeature

Later on you can switch between multiple branches using the git checkout
command:

.. code-block:: console

   git checkout myFeature

Every git project has a branch called *master* that the owners of the
repository consider the main branch. Usually, nothing gets added to this
branch until it is tested in a variety of environments and the project
leaders are sure it's both robust and useful. 

Tracking Remote Branches
~~~~~~~~~~~~~~~~~~~~~~~~

There are two types of branches in git, *local* and *remote*. Remote
branches are branches that exist in the remote repository. You can also
track remote branches using the following command:

.. code-block:: console

   git checkout --track remote_repository:remote_branch

For instance, to checkout the KDE 4.7 branch, use:

.. code-block:: console

   git checkout --track origin:KDE/4.7

Committing your work
--------------------

Before pushing anything to the KDE codebase, you need a KDE identity and
a developer account. Please visit http://identity.kde.org/ to register
your account. Your account name will almost always be your surname; do
not attempt to get around this rule. Developers with a lot of experience
can get accounts with commit rights, but that is outside the scope of
this book.

In order to generate a diff of your changes and review them, run the
following:

.. code-block:: console

   git diff

You can pipe this output into a patch using the following:

.. code-block:: console

   git diff > my_patch.patch

In order to commit your work, you first need to add the files you
changed to the staging area using the following command:

.. code-block:: console

   git add fileName1 fileName2

In order to commit your changes to your local repository, use the
following command:

.. code-block:: console

   git commit

This opens up a text editor where you can type a commit message that
describes your changes.

Submitting your changes into the main tree
------------------------------------------

You can submit your patches to the KDE Review Board
at https://git.reviewboard.kde.org/
Log in using your KDE Identity account and submit a patch to a project
there.

You can also directly send patches to review board using post review.
The procedure is outlined at:

   http://community.kde.org/Sysadmin/GitKdeOrgManual#Using_Review_Board_and_post-review

Troubleshooting
---------------

For any issues related to git and the KDE Development Platform, you can
seek help in one of the following channels on irc.freenode.net.

-  #kde-sysadmin
-  #kde-git

You can read more about KDE platform development using git
at http://techbase.kde.org/Development/Git.
