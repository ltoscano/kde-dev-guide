KDE APIs
========

An Application Programming Interface (API) is a set of functions exposed
by a program or library that can be invoked by other programmers. An
API greatly extends a program by allowing third-party developers to add
new functionality. It is an ideal way to allow new features to be added
without the need to modify the existing, core code.

The documentation for an API explains how things work and what methods
can be called. For some nice tutorials to get started with using the
myriad KDE development platform APIs, visit:

   http://techbase.kde.org/Development/Tutorials

The KDE development platform API documentation (apidox) can be found at:

   `http://api.kde.org <http://api.kde.org/>`__

This documentation comes in the qch format, an extended HTML format that
can be viewed either in the QtCreator Assistant or via any browser.

The apidox are processed by the Doxygen documentation tool. This tool
reads source code for an application (or library) and produces nicely
formatted documentation from it. There is a good reference manual
available, but hopefully you won't need to consult it just for basic
documentation building. When you install the KDE development platform
for development purposes, you should also install Doxygen. Then you can
build documentation from any project by using the following command:

.. code-block:: console

   make docs

More information on the KDE API docs can be found at:

   http://techbase.kde.org/Development/Tools/apidox
