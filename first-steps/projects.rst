Choosing a project
==================

When you come to KDE as a developer, you may already have a favorite
project and know how you want to contribute. But it's worth looking over
the various projects listed in this chapter, to find out all the ways
you may be able to help. And even if you're really only interested in
one project, it's useful to know what others are active because your
work may interact with them.

Frameworks
----------

These are general components underlying the applications and other
visible parts of KDE. The team is working hard to make the
libraries modular, clarify the dependencies, simplify, and increase the
quality and stability.

KDE Core Libraries (kdelibs)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Critical functions needed across the KDE platform

Widgets & Classes
~~~~~~~~~~~~~~~~~

   Widgets and classes that are not in kdelibs but that are widely
   useful

D-Bus Web Service Proxy
~~~~~~~~~~~~~~~~~~~~~~~

   A project to connect Web Services to the D-Bus notification framework
   on Linux

kdesu
~~~~~

   Tools for gaining superuser privileges on different backends

KDE WebKit
~~~~~~~~~~

   | A project integrating the QtWebKit browser engine into the KDE
     Software Compilation

KioFuse
~~~~~~~

   This inserts KIO(K Input Output) resources (remote, archived, or
   compressed files) into the root filesystem hierarchy

Nepomuk
~~~~~~~

   Basic desktop operations for annotation, indexing, search, and
   linking

Network Management
~~~~~~~~~~~~~~~~~~

   An applet and configuration tool for Solid Networking and
   KNetworkManager

Oxygen
~~~~~~

   Artwork for the KDE SC

Solid
~~~~~

   The KDE SC hardware library

Telepathy
~~~~~~~~~

   A communications framework for Instant Messaging, VoIP, and
   Collaboration

Akonadi
~~~~~~~

   An extensible cross-desktop storage service for PIM data and meta
   data

Related projects
~~~~~~~~~~~~~~~~

   Non-central projects that are related to KDE technology in various
   ways, such as dependencies or build tools

Programs
--------

This is a small cutout of the applications created and maintained by KDE
developers.

Amarok
~~~~~~

   Amarok’s tagline is *Rediscover Your Music*, and its development is
   based around this ideology. Amarok’s core features such as the unique
   *context browser*, integrated Wikipedia lookup and lyrics download
   help users to find new music, and to learn more about the music they
   have.

Digikam
~~~~~~~

   Photo management software

Gwenview
~~~~~~~~

   KDE image viewer

K3b
~~~

   An optical disc writer

KDevelop4
~~~~~~~~~

   Integrated Development Environment (IDE) for KDE SC

KWin
~~~~

   The KDE Window Manager

Marble
~~~~~~

   A visually appealing globe program

Okular
~~~~~~

   A unified document viewer

Rekonq
~~~~~~

   A lightweight web browser powered by WebKit and KDE SC

System Settings
~~~~~~~~~~~~~~~

   The System Settings configuration tool.

SuperKaramba
~~~~~~~~~~~~

   A tool for creating attractive widgets and other interactive elements

Suites
------

These projects group many related applications. The key concept is that
data created in one part of the suite can be easily used by another
application in the suite.

KDE Education Project
~~~~~~~~~~~~~~~~~~~~~

   Educational software for KDE

KDE Finance
~~~~~~~~~~~

   Financial applications

KDE Games
~~~~~~~~~

   Desktop games for KDE

Kdetoys
~~~~~~~

   A set of amusing diversions

KDE Utilities
~~~~~~~~~~~~~

   A variety of tools to run on the desktop

KOffice and Calligra
~~~~~~~~~~~~~~~~~~~~

   Office suites based on KDE libraries

PIM
~~~

   Personal information management tools

Plasma
~~~~~~

   Programs for the quick and easy creation of widgets, including
   interactive application launchers, and window and task managers

Platforms
---------

These projects ensure that KDE works on various operating systems.

KDE on Mac OS X
~~~~~~~~~~~~~~~

   KDE libraries and applications for Mac OS X

KDE on Windows
~~~~~~~~~~~~~~

   KDE libraries and applications for Microsoft Windows

KDE on FreeBSD
~~~~~~~~~~~~~~

   KDE libraries and applications on FreeBSD and other BSD versions

Plasma Active
~~~~~~~~~~~~~

   A project for porting KDE technology to mobile devices

Working with the organisation
-----------------------------

These projects deal with the people and processes that make KDE
possible.

KDE Release Team
~~~~~~~~~~~~~~~~

   Schedules and coordinates releases

KDE Documentation Project
~~~~~~~~~~~~~~~~~~~~~~~~~

   Creates and maintains KDE documentation

kde.org
~~~~~~~

   Provides information around the \*.kde.org websites

KDE Promotion
~~~~~~~~~~~~~

   Promotes KDE and organizes conferences

Partner Program
~~~~~~~~~~~~~~~

   Supports KDE partner Independent Software Vendors (ISVs)

KDE Usability project
~~~~~~~~~~~~~~~~~~~~~

   Applies usability principles and practices to the K Desktop
   Environment

KDE Accessibility project
~~~~~~~~~~~~~~~~~~~~~~~~~

   Builds on Qt features for making interactive environments more
   accessible to the disabled or others with special needs

KDE BugSquad
~~~~~~~~~~~~

   Keeps track of incoming bugs in KDE software, and goes through old
   bugs.

Summer of Code Projects
~~~~~~~~~~~~~~~~~~~~~~~

   Google Summer of Code projects related to KDE

English Breakfast Network (EBN)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Provides tools dedicated to KDE Code Quality, including KDE API
   Documentation Validation, User Documentation Validation, Source Code
   Checking, etc.

KDE Research
~~~~~~~~~~~~

   Supports everyone who is interested in contributing to (funded)
   research projects with(in) the KDE community.

If you are still confused which project you want to work with then
try hanging out with KDE SC developers on IRC to become familiar with
the project.
