The Qt Framework
================

To start developing on the KDE Development Platform you will need to get
familiar with the Qt framework, which is one of building blocks of KDE
development.

Qt (pronounced officially as cute) is a cross-platform application
framework based on C++, that is widely used for developing application
software with a graphical user interface (GUI). Thus, it is largely a
widget toolkit, but is also used for developing non-GUI programs such as
command-line tools and consoles for servers.

Besides the KDE Development Platform, Qt is most notably used in
Autodesk Maya, Adobe Photoshop Elements, OPIE, Skype, VLC media player,
VirtualBox, and Mathematica, and by the European Space Agency,
DreamWorks, Google, HP, Lucasfilm, Panasonic, Philips, Samsung,
Siemens, Volvo, and Walt Disney Animation Studios.

Advantages of Qt
----------------

Writing code once to target multiple platforms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Qt allows you to write advanced applications and UIs that you can deploy
across different desktops and embedded operating systems without
rewriting the source code, saving time and development cost.

Creating amazing user experiences
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Whether you prefer C++ or JavaScript, Qt provides the building blocks
for modern, interactive interfaces: a broad set of customizable widgets,
graphics canvas, style engines, and more. You can incorporate 3D
graphics, multimedia audio or video, visual effects, and animations to
set your application apart from the competition.

Doing more (and faster!) with less
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Qt is fast to learn and to use, particularly when used together with the
new Qt Creator cross-platform IDE. And Qt's modular class library
provides much of the necessary infrastructure for interactive
applications.

Blending web and native code in a single application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Qt's integration with the WebKit web rendering engine means that you can
quickly incorporate content and services from the Web into your native
application, and can use the web environment to deliver your services
and functionality.

To learn how to use Qt, we recommend the tutorials here:

   http://doc.qt.nokia.com/
