Documentation
=============

If you write new functions for a KDE project, we certainly hope that you
write documentation (apidox) for it, and try to explain as clearly as
possible what can be done with the functions. The better you document
your work, the more developers are likely to use it and the less likely
they are to annoy you by asking basic questions about how it works.

API documentation is sometimes called an API reference manual.
It needn't be just a reference manual, though. It can contain
extensive additional material, such as tutorials, examples, and
historical information. This page refers to all the material that
documents and explains the API of a piece of code as "apidox", the term
used in the KDE development platform documentation itself.

Basic apidox writing is fun and simple: add specially formatted comments
in your code explaining what things are supposed to be for. These
comments are nearly indistinguishable from stuff you would be writing in
the headers of your code anyway, so that's not hard to do. Here is a
sample.

.. code-block:: cpp

   /**
    * @author praxagora
    *
    * Returns a CHI square comparing two samples.
    * The arrays must be the same size.
    *
    * @param a First sample
    * @param b Second sample
    *
    * @return @size size of each sample.
    */
   double chi_square(double a[], double b[], int size) {
   double total_a=0.0, total_b=0.0, total, totals[size],
   square_a[size], square_b[size], square_totals[size],
       terms[size],
       terms_total=0.0, sum_total=0.0, square_sum_total=0.0;
     int i;
     /**
       * This loop comprises the whole function and computes the CHI
       * square from the two arrays.
     */
       {
     for (i = 0; i < size ; i++)

Note that a comment was included before the function, and before the
bracket that starts the next level of nesting, a for loop. All the
parameters, as well as the return value, are marked.

To write good apidox, you first need technical knowledge: you should
understand the code you are documenting--or at least know what it is
supposed to do and how it is meant to be used. The other part of good
apidox is plain discipline: apidox are most useful when they are
exhaustive.

Actually, documentation doesn't usually have to explain what the code
does at each step. The code should be so clear, with a clean layout and
well-chosen names for methods and variables, that it is
self-documenting. Rather, documentation should explain what code is
*for*, when and why it is called, the purposes and ranges of its
arguments, and perhaps the algorithm used and the trade-offs made in
memory and time.

Look at the Qt documentation to get a feeling for what good apidox look
like. They have a consistency of style and are permeated with a concern
for thoroughness. You can learn a lot about Qt just from reading the
documentation. You do not necessarily need to run the tutorial programs
or read the source code of the library to find out what a parameter or
flag does in some method of the library. It is all spelled out for you.
