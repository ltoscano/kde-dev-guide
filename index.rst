.. KDE Dev Guide documentation master file, created by
   sphinx-quickstart on Wed Aug 15 16:25:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the KDE Dev Guide
============================

.. toctree::
   :maxdepth: 2

   intro/index
   first-steps/index
   build-environment/index
   advanced/index
   appendices/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
