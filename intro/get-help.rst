How to get help
===============

Of course, we hope our book is helpful to you! But inevitably, you will
come up with questions or need help with a problem. The KDE community is
helpful and friendly, so pick the most appropriate method in this
chapter and ask away. Unless you have a specific need to talk to one
particular person, please address the whole list or channel.

KDE Mailing Lists
-----------------

The KDE mailing lists are one of the main communication channels in the
KDE Community. All developers will find the KDE-devel list useful. In
addition, those working on the core of the KDE Software Compilation (SC)
will want to subscribe to KDE-Core-devel. Those working on applications
or KDE projects should subscribe to the project mailing list. The full
spectrum of KDE lists can be found at
https://mail.kde.org/mailman/listinfo.

Both help and information are available on the lists. However, if you
need help quickly, IRC may be more useful.

KDE on IRC
----------

IRC is Internet Relay Chat, a text-only, real-time communication tool.
There are a variety of IRC clients available from KDE, such as
Konversation, Quassel, and Kvirc.

.. image:: /images/IRC_1.png


Almost all KDE developers show up or idle on KDE IRC channels on
Freenode (irc.freenode.net). IRC is the best way to get quick help from
the KDE developers. As a developer, you will want to be in #kde,
#kde-devel and your project channel or channels. You'll find that the
more time you spend in IRC, the more you will get to know your fellow
developers and our KDE users. Life-long friendships have started in KDE
channels. Help is available about the services such as chanserv and
nickserv by using the commands */msg chanserv help* and */msg nickserv
help*. More about using Freenode is available
here: http://freenode.net/using_the_network.shtml

Userbase keeps a list of channels current
at http://userbase.kde.org/IRC_Channels, and you can also use the IRC
command */msg alis list $searchterm*, where *$searchterm* is the
subject in which you are interested.

When asking questions, please bear these tips in mind:

-  If you have a question, just ask it. There's no need to ask first
   whether you can ask a question.
-  Be prepared to wait for an answer. Even though IRC is a more
   real-time mode of communication than mailing lists, there may not be
   anyone available to answer your question immediately. In general, if
   you don't receive a response on IRC in about an hour, it's best to
   send an email.
-  Don't ask your question more than once. Even though the channel is
   active, the right person may not be available to provide an answer.
   The one exception to this rule is that, if you are told to wait for a
   certain person to come online, ask again when you see him or her come
   online. Again, if you don't receive a response to your question in
   about an hour, it's probably best to send an email.
-  Pasting large amounts of text is considered bad etiquette, so use a
   pastebin. KDE's pastebin is at http://paste.kde.org

KDE Community Problems
----------------------

If you encounter bad behavior on a list or in a channel, please contact
the list owners or one of the ops. The list owner address is
*$listname*-owner@kde.org, where *$listname* is the name of the list.
Ops in a channel can be identified by issuing this command: */msg
chanserv access #channelname list*. Ops will have a "+" next to their
nicks. For general KDE community help, please write to the Community
Working Group at *community-wg@kde.org*, or stop by our IRC channel at
#kde-cwg.
