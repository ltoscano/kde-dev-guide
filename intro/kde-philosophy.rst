The KDE Philosophy
==================

KDE's success is based on a world-view that we've found to be both
practical and motivating. Elements of this development philosophy
include-

**Using available tools rather than re-inventing existing ones**: Many
of the basics you need to do your work are already part of KDE, such as
the core libraries or Kparts, and are quite mature. So check out all
available resources before trying to solve your own problem.

**When making a suggestion, change we should.. to I will..**: Grandiose
plans are useless unless you are willing to put in the work to make them
happen. You will find help once you start!

**Improve code iteratively**: Don't let the perfect be the enemy of the
good. Try a small solution, get it to work, and improve it through
testing and refactoring to produce an excellent patch.

The KDE Community
-----------------

The KDE development platform is created and maintained by an
international team that cooperates on the development and distribution
of free, open source software for desktop and portable computing. Our
community has developed a wide variety of applications for
communication, work, education, and entertainment. We have a strong
focus on finding solutions to old and new problems, creating an open
atmosphere for experimentation.

As a community, we value all contributions, and can use all member
talents, including artwork, graphics, design, communication,
translations, documentation, testing, bug-reporting and bug-hunting,
system administration, and coding.

What makes KDE so exciting?
---------------------------

The best thing about KDE is our amazing community! We welcome new
members, offering help and allowing people to experiment, learn and
grow. This book is a part of that mission.

Our products are used by millions of home and office workers, and are
being deployed in schools around the world. Brazil alone has over 50
million school children using KDE-based software to browse, learn and
communicate! As a complement to Google Summer of Code, we run our own
Season of KDE, where people take on the responsibility of working on a
project intensively for the summer, and get a cool T-shirt at the end,
in thanks. In 2011, we had 100 proposals! What an amazing community.

Is it difficult to get involved?
--------------------------------

Not at all! Every day, more and more people join our ever-growing family
of contributors. KDE has a strong infrastructure of web resources,
forums, mailing-lists, IRC, and other communication services. We can
provide feedback on your code and other contributions with the goal of
helping you learn. Of course, a proactive attitude and a friendly
personality helps.

.. image:: /images/SolidSprint2011.jpg

The KDE Code of Conduct
-----------------------

When communicating in official KDE channels please observe the KDE Code
of Conduct.

Our Code of Conduct presents a summary of the shared values and *common
sense* thinking in our community. The basic social ingredients that hold
our project together include:

-  Be considerate
-  Be respectful
-  Be collaborative
-  Be pragmatic
-  Support others in the community
-  Get support from others in the community

The Code of Conduct can be found here
: *http://www.kde.org/code-of-conduct/*

The value of being free and open source
---------------------------------------

Our code can be copied, changed, and redistributed by anybody. This
means you can be confident that the software will continue to be
available even if the original developers move on. It also means that
development is out in the open, with comments from anyone in the world
who is interested. Finally, open source code makes it easier for a
developer to find and fix problems. One of the practical effects of free
software is that wonderful communities tend to develop around code. We
have created all this within KDE.

The KDE community is involved with the larger free and open source
movement. We cooperate with the GNOME community in FreeDesktop.org and
the bi-annual Desktop Summit. We've been a leading participant in Google
Summer of Code and Google Code-In since they began, and we regularly
sprint with members of other FOSS groups, and participate in open
standard efforts. We are supported in these efforts by the KDE e.V., who
issue quarterly reports of their activity
at *http://ev.kde.org/reports/*.

.. image:: /images/Randa2011Sprint.jpeg
