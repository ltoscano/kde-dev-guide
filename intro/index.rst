================================
KDE from a developer's viewpoint
================================

.. toctree::
   :maxdepth: 2

   book-intro
   kde-philosophy
   get-help
