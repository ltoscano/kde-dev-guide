Do you need this book?
======================

You should read this book if you want to do any development for KDE.
We're using the term *development* very broadly to cover anything that
can lead to a change in source code. This includes:

-  Submitting a bug fix
-  Writing a new application powered by KDE technology
-  Contributing to an existing project
-  Adding functionality to KDE development libraries

In this book we'll give you the basics you need to be a productive
developer. We'll explain the tools you should install, show you how to
read the documentation (and write your own, once you've created new
functionality!) and how to get help in other ways. We'll introduce you
to the KDE community, which is key to understanding KDE because we are a
free, open source project.

Regular users of the software do **NOT** need this book! However, they
might find it interesting to help understand how the complex and richly
featured software they use has come into being.

.. image:: /images/powered_by_kde_vertical_115.png
