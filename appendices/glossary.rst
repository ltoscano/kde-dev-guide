General glossary
================

.. rubric:: A

**Accessibility (a11y)**:The ability of all people, regardless of
disability or severity of impairment, to use the features of a program.

**Akonadi**: KDE extensible cross-desktop storage service for personal
information management (PIM) data and metadata providing concurrent
read, write, and query access. Provides desktop-wide object
identification and retrieval.

**Akregator**: KDE open source feed aggregator, supporting both RSS and
Atom. Feeds can be sorted into categories, and there is an incremental
search feature for the titles of all the entries in the database.

**Algorithm**: step-by-step procedure for calculations, data processing,
and automated reasoning.

**Amarok**: *Rediscover Your Music* using KDE's Amarok. Core features
such as the unique *context browser*, integrated Wikipedia lookup and
lyrics download help users to find new music, and to learn more about
the music they have.

**Application programming interface (API)**: A particular set of coding
rules and specifications that software programs can follow to
communicate with another. It serves as an interface between different
software programs and facilitates their interaction, similar to the way
the user interface facilitates interaction between humans and computers.

**apidox**: API Documentation

**Applet**: Program written in Java to be embedded in another
environment, such as a Web page.

.. rubric:: B

**Backtrace**: also called *stack backtrace, stack trace* or *stack
traceback*. This is a report of the active stack frames at a certain
point in time during the execution of a program. When using KDE
software, one gets the best backtrace from Dr. Konqui, if it pops up
after a crash. Otherwise, GDB can be used to get a backtrace.

**Bot**: Software applications that run automated tasks over the
Internet, or in IRC channels.

**Bug**: An error, flaw, mistake, failure, or fault in a computer
program or system that produces an incorrect or unexpected result, or
causes it to behave in unintended ways.

**Bug Squad**: The team that keeps track of incoming bugs in KDE
software.

**Bugzilla**: A web-based, general-purpose bugtracker and testing tool,
used by the KDE community.

**Build**: Short for software build, which refers either to the process
of converting source code files into standalone software artifact(s)
that can be run on a computer, or the result of doing so. One of the
most important steps of a software build is the compilation process,
which converts source code files into executable code.

**Build system**: Software tools that script or automates a wide variety
of tasks that software developers do in their day-to-day activities,
such as compiling computer source code into binary code, packaging
binary code, running tests, deploying code to production systems, and
creating documentation and/or release notes.

.. rubric:: C

**Channel, IRC channel**: The basic place to ask questions and get help
in IRC. It is rude to direct questions to one person rather than asking
the help channel in general.

**CMake**: an open-source build system that enables developers to
automate compiling, testing and packaging of software based on
specifications written in text files.

**Code**: Text written in a computer programming language.

**Commit**: To make a set of tentative changes permanent.

**Compile**: To use a compiler to process source code into executable
code. Also a destination for messages logged by programs or the
operating system, where administrators or developers can view the
messages.

**Console**: A command-line interface (CLI).

**C++**: The coding language in which KDE software is primarily built.

.. rubric:: D

**Debugger**: A computer program used to test and find bugs in other
programs (the *target* programs).

**Desktop**: In graphical computing, a desktop environment (DE) commonly
refers to a style of graphical user interface (GUI) derived from the
desktop metaphor that is seen on most modern personal computers. The
most popular modern Linux desktops are the KDE workspaces and GNOME.

**Dependency**: A package you need to install in order for your
application to build and run.

**Diff**: A file comparison utility that outputs the differences between
two files. Also refers to the output of such a program, which can be
called a patch (since the output can be applied with the Unix program
*patch*).

**digiKam**: an image organizer and editor that uses the KDE Platform.
It runs on most known desktop environments and window managers, supports
all major image file formats, and can organize collections of
photographs in directory-based albums, or dynamic albums by date,
timeline, or tags. Users can also add captions and ratings to their
images, search through them and save searches for later use. With the
plugins one can also export albums to 23hq, Facebook, Flickr, Gallery2,
Google Earth's KML files, SmugMug, Piwigo, and Simpleviewer, or burn
them onto a CD, or create a web gallery.

**Distributed repository**: A peer-to-peer approach to sharing and
maintaining code or other collaborative work, in contrast to the
client-server approach of centralized systems. Rather than a single,
central repository on which clients synchronize, each peer's working
copy of the codebase is a separate repository bound by a web of trust.

**Distribution (Distro)**: a selection of packages that make up a
working software system, and provided together to the user. Often
applied to the GNU/Linux system in particular.

**Docs**: *documentation,* an essential part of the development process.

.. rubric:: E

**EBN**: English Breakfast Network, a site dedicated to the
contemplation of tea, KDE API Documentation Validation, User
Documentation Validation, Source Code Checking, omphaloskepsis, and
star-gazing.

**Eclipse**: A multi-language software development environment
comprising an integrated development environment (IDE) and an extensible
plug-in system. It is written mostly in Java and can be used to develop
applications in Java and, by means of various plug-ins, other
programming languages including Ada, C, C++, COBOL, Perl, PHP, Python,
R, Ruby (including the Ruby on Rails framework), Scala, Clojure, Groovy,
and Scheme.

**Environment**: A set of configuration scripts and bash commands
provided as a recommended configuration when building KDE software
manually.

**e.V.**: *Eingetragener Verein*, a registered voluntary association in
Germany. The KDE e.V.is a registered non-profit organization that
represents the KDE Community in legal and financial matters.

.. rubric:: F

**Forum**: An online discussion site where people can hold conversations
by posting messages. They differ from chat rooms in that messages are
archived. A discussion forum is hierarchical or tree-like in structure:
a forum can contain a number of subforums, each of which may have
several topics. Within a forum's topic, each new discussion started is
called a thread, and can be replied to by anyone who wishes to.

**FOSS:** Free and open-source software (F/OSS, FOSS) or
free/libre/open-source software (FLOSS, FL/OSS) is software that is
liberally licensed to grant users the right to use, study, share,
change, and improve its design through access to its source code.

**Framework**: An abstraction in which software providing generic
functionality can be selectively changed by user code, thus providing
application specific software. It is a collection of software libraries
providing a defined application programming interface (API).

**Frameworks**: Beginning with KDE Frameworks 5.0, KDE has a roadmap for
the next major releast of KDE's libraries and runtime requirements, with
an emphasis is on modularity, dependency clarity, simplification and
increasing quality.

**Freenode**: An IRC network used to discuss peer-directed projects.

.. rubric:: G

**GDB**: GNU Debugger, usually called just GDB and named gdb as an
executable file. This is the standard debugger for the GNU software
system.

**Git**: a distributed revision control system with an emphasis on speed
and support for multiple branches shared among many developers. Every
Git working directory is a full-fledged repository with complete history
and full revision tracking capabilities, not dependent on network access
or a central server. Free software distributed as GPL v.2.

**Gluon**: a way of creating and playing games, and a means for players
and makers of games to get together and talk about their shared
interest. You can use the powerful Gluon Creator to build the games,
interact with other makers and players of games on the GamingFreedom.org
network site, and play them on any of the many supported platforms with
one of the Gluon Player applications.

**GPL**: The GNU General Public License is a free, copyleft license for
software and other kinds of works.

.. rubric:: H

.. rubric:: I

**Integrated development environment (IDE)**: a software application
that provides comprehensive facilities to computer programmers for
software development. An IDE normally consists ofa source code
editor,a compiler and/or an interpreter,build automation tool, anda
debugger. (Also known as *integrated design
environment*, *integrated debugging environment*, or *interactive
development environment*.)

**Internationalization (i18n)**: The insertion of constructs that make
it easy to change the interface and language of a program for different
cultures and countries (see also **Localization**).

**Internet Relay Chat (IRC)**: A text-based real-time communication
tool. KDE channels are on *irc://irc.freenode.net*.

.. rubric:: J

.. rubric:: K

**Kate**: a text editor included in the KDE SC. The name Kate is an
acronym for KDE Advanced Text Editor.

**KDE PIM**: KDE Personal Information Management, such as Kontact,
KMail, KOrganizer, etc. Also, a work group within the larger KDE SC
project that develops the individual Kontact applications in a
coordinated way.

**KDE SC**: KDE Software Compilation, the sources for the KDE
distribution

**KMail**: KDE email client that supports folders, filtering, viewing
HTML mail, and international character sets. It can handle IMAP, IMAP
IDLE, dIMAP, POP3, and local mailboxes for incoming mail. It can send
mail via SMTP or sendmail.

**Konqueror**: KDE web browser and file manager. Provides file-viewer
functionality to a wide variety of things: local files, files on a
remote ftp server and files in a disk image.

**Konsole**: a free terminal emulator that is part of KDE SC. The KDE
applications Konqueror, Krusader, Kate, Konversation, Dolphin and
KDevelop use Konsole to provide embedded terminal functionality.

**Kontact**: KDE's personal information manager and groupware software
suite. Supports calendars, contacts, notes, to-do lists, news, and
email. Uses KParts to embed the various applications (KMail,
KAddressBook, Akregator, etc.) into the container application.

**Kopete**: KDE's multi-protocol, free software instant messaging
client.

**Kpackage Kit**: KDE's frontend for PackageKit.PackageKit is an open
source suite of software applications designed to provide a consistent
and high-level front end for a number of different package management
systems.

**Kparts**: component framework for the KDE SC. For example, Konsole is
available as a KPart and is used in applications like Konqueror and
Kate.

**Konversation**: user-friendly Internet Relay Chat (IRC) client built
on the KDE Platform.

**KWin**: the window manager that is an integral part of the KDE SC. It
can also be used on its own or with other desktop environments.

.. rubric:: L

**LAMP**: acronym for a software bundle or platform consisting of Linux,
Apache, MySQL and Perl/PHP/Python.

**Licensing**: legal instruments (usually by way of contract law) that
govern the usage or redistribution of software. All software is
copyright protected, except material in the public domain.

**Localization (l10n)**: making the changes required to display a
program's interface using the language and conventions of a particular
country (see also **Internationalization**).

.. rubric:: M

**Mailing list**: A collection of names and addresses used by an
individual or an organization to send material to multiple recipients.
Often extended to include the people subscribed to such a list, so the
group of subscribers is referred to as *the mailing list*, or simply
*the list*.

.. rubric:: N

**Nepomuk**: KDE workspaces project that supports annotations, indexing,
search, and linking.

**Nick**: a user's screen name or online handle.

**Nightly**: a neutral build that reflects the current state of the
source code checked into the version control system by the developers,
as built in a neutral environment (that is, in an environment not used
for development). A nightly build is a neutral build that takes place
automatically, typically each night. Project Neon is such a project for
KDE.

.. rubric:: O

**Object-oriented programming (OOP)**: a programming paradigm using
*objects* – data structures consisting of data fields and methods
together with their interactions – to design applications and computer
programs.

**Ocular**: KDE's universal document viewer based on KPDF.

**Operators (IRCops)**: Channel operators have powers over the IRC
channel, including moderating or kicking out disruptive users. *IRCops
or sysops* control the IRC server, so they control the channels as well
as having control over who can participate. On most systems, ops are
identified with a symbol next to their nicks, but Freenode discourages
ops from appearing as such unless they have work to do in the channel.

.. rubric:: P

**Package, packaging**: There are two types of packages that may be
downloaded from the KDE FTP site: binary packages (rpms, debs, and the
like) and source packages. Binary packages are compiled ("runnable")
versions of KDE SC that are built to run on a specific OS or
distribution. Source packages are the raw code that makes up KDE SC, and
need to be compiled before they can be used. KDE software packages
available from the distributions may be slightly different from the pure
KDE source packages.

**Pastebin**: a web application that allows users to upload snippets of
text, usually samples of source code, for public viewing.Use is
encouraged in IRC channels, where pasting large amounts of text is
considered bad etiquette. KDE's pastebin is hosted at
http://paste.kde.org

**Patch**: software designed to fix problems with, or update a computer
program or its supporting data. This includes fixing security
vulnerabilities and other bugs, and improving the usability or
performance.

**Phonon**: multimedia API provided by Qt; the standard abstraction for
handling multimedia streams within the KDE SC.

**Plasma Active**: the latest initiative of the Plasma team, bringing
KDE functionality to mobile devices.

**Plasma**: KDE SC framework to facilitate the creation of widgets.
These cover interactive application launchers, window and task managers,
and more.

**Plasmoid**: widget in thePlasma Desktop environment.

**Post-mortem debugging:** Debugging after a crash report has been
filed.

.. rubric:: Q

**QtCreator**: An integrated, cross-platform IDE for C++ and JavaScript
that is part of the Qt SDK.

**Qt**: Cross-platform application framework that is widely used for
developing application software with a graphical user interface (GUI).

**Quassel**: cross-platform, distributed IRC client, meaning that one
(or multiple) client(s) can attach to and detach from a central core --
much like the popular combination of screen and a text-based IRC client
such as WeeChat, but graphical.

.. rubric:: R

**Rekonq**: KDE web browser based on WebKit.

**Reviewboard**: web-based collaborative code review tool, available as
free software under the MIT License. An alternative to Rietveld and
Gerrit, Review Board integrates with Bazaar, ClearCase, CVS, Git,
Mercurial, Perforce, and Subversion.

**Review**: systematic examination (often as peer review) of computer
source code. It is intended to find and fix mistakes overlooked in the
initial development phase, improving both the overall quality of
software and the developers' skills.

.. rubric:: S

**Script**: small program written for a command interpreter or another
scripting language.

**Server**: computer program running to serve the requests of other
programs, the *clients*.

**Solid**: device integration framework for KDE SC. It functions on
similar principles to KDE's multimedia pillar Phonon; rather than
managing hardware on its own, instead it makes existing solutions
accessible through a single API.

**Source**: Human-readable instructions in a programming language, to be
transformed into machine instructions by a compiler, interpreter,
assembler or other such system.

**Sprint**: face-to-face meeting of team members who usually work
together remotely.

**Suite**: collection of computer programs, usually application software
and programming software of related functionality, often sharing a
more-or-less common user interface and some ability to smoothly exchange
data with each other.

**Summit**: in KDE and FOSS, a large meeting for members who usually
work remotely. Team sprints may take place before, during and after a
large summit.

**SVN (Subversion)**: A software versioning and a revision control
system distributed under a free license, part of the Apache Foundation.

.. rubric:: T

**Techbase**: KDE's developer documentation wiki.

**Telepathy**: A realtime communication framework that supports instant
messaging, VoIP, and collaboration.

**Terminal**: interface for serial entry and display of textual data.
See also **console**.

**Testing**: investigation conducted to provide stakeholders with
information about the quality of the product or service under test.

**Text editor**: program used for editing plain text files.

**Toolchain**: set of programming tools that are used to create a
product (typically another computer program or system of programs). The
tools may be used in a chain, so that the output of each tool becomes
the input for the next, but the term is used widely to refer to any set
of *linked development tools*.

**Toolkit**: set of basic building units for graphical user interfaces.
KDE SC uses the Qt toolkit.

**Trunk**: the unnamed branch (version) of a file tree under revision
control. The trunk (or *master*) is usually meant to be the base of a
project on which development progresses.

.. rubric:: U

**Unit tests**: method by which individual units of source code are
tested to determine if they are fit for use. A unit is the smallest
testable part of an application. In object-oriented programming a unit
is usually an interface, such as a class.

**Usability**: ease of use and learnability of a human-made object, in
this case, our software.

**Userbase**: KDE's user documentation wiki.

.. rubric:: V

**Valgrind**: GPL licensed programming tool for memory debugging, memory
leak detection, and profiling. The name valgrind comes from the main
entrance to Valhalla in Norse mythology.

**Variable**: symbolic name given to some known or unknown quantity or
information, for the purpose of allowing the name to be used
independently of the information it represents. A variable name in
computer source code is usually associated with a data storage location
and thus also its contents, and these may change during the course of
program execution.

**Version control**: Revision control, also known as version control and
source control (and an aspect of software configuration management or
SCM), is the management of changes to documents, programs, and other
information stored as computer files. It is most commonly used in
software development, where a team of people may change the same files.
Changes are usually identified by a number or letter code, termed the
"revision number", "revision level", or simply "revision".

.. rubric:: W

**Widget**: element of a graphical user interface (GUI) that displays an
information arrangement changeable by the user, such as a window or a
text box. The defining characteristic of a widget is to provide a single
interaction point for the direct manipulation of a given kind of data.
In other words, widgets are basic visual building blocks which, combined
in an application, hold all the data processed by the application and
the available interactions on this data.

**Wiki**: website that allows the creation and editing of any number of
interlinked web pages via a web browser using a simplified markup
language or a WYSIWYG text editor. Wikis are typically powered by wiki
software and are often used collaboratively by multiple users.

**Word processor**: computer application used for the production
(including composition, editing, formatting, and possibly printing) of
any sort of printable material.

.. rubric:: X

**X, X window system**: computer software system and network protocol
that provides a basis for graphical user interfaces (GUIs) and rich
input device capability for networked computers. It creates a hardware
abstraction layer where software is written to use a generalized set of
commands, allowing for device independence and reuse of programs on any
computer that implements X. **X.Org** serves as the canonical
implementation of X, and is what KDE SC uses.

.. rubric:: Y

**Yakuake**: drop-down terminal emulator based on KDE Konsole
technology.

.. rubric:: Z

**Z-machine**: virtual machine used by Infocom for its text adventure
games. Kwest is a Z-machine interpreter for KDE.
