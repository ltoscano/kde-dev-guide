Useful links
============

KDE: http://kde.org/
   The KDE Project home page

KDE Techbase: http://techbase.kde.org/
   The KDE Development wiki

KDE Userbase: http://userbase.kde.org/
   The KDE User wiki

KDE Identity: https://identity.kde.org/
   A single sign-in system used across several KDE websites

KDE Projects Page: https://projects.kde.org/
   An overview of all projects within git.kde.org that are based on KDE
   technology

Qt Tutorials: http://doc.qt.nokia.com/
   An extensive reference to Qt documentation

Git in 30 minutes: http://blip.tv/scott-chacon/git-in-30-minutes-4064151
   A useful video that introduces the concepts revolving around git

The Dot: http://dot.kde.org/
   The official KDE community news outlet

Behind KDE: http://behindkde.org/
   People Behind KDE interviews the people who work on KDE

Planet KDE: http://planetkde.org/
   Aggregation of KDE community member blogs

The KDE Release Schedule: `http://techbase.kde.org/Schedules <http://techbase.kde.org/Schedules/Release_Schedules_Guide>`__

