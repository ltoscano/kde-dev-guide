KDE Developer Guide Frequently asked questions (FAQ)
====================================================

Q. I want to help/develop the KDE SC. How do I start?

A. A great way to get familiar with the codebase is to fix some bugs.
You can find a number of "Junior Jobs" on the KDE Bugzilla. Search for
the "Junior Jobs" link on the KDE bugzilla
(`http://bugs.kde.org <http://bugs.kde.org/>`__) on the left.


Q. I do not understand what [insert strange term here] means.

A. Check the term in the *General Glossary and KDE Jargon*.


Q. What project should I contribute to?

A. Find something that interests you, and start hacking! Most people
start by "scratching their own itches" first, which means that they
try to fix the bugs that irritate them the most. See also the list of
KDE SC projects in the section *Choosing a project*.


Q. How do I set up the Development Environment?

A. This is documented in the *KDE Development build
environment* section of this book.


Q. What programming languages do I need to know?

A. Most of the KDE SC is written in C++, but KDE SC has bindings for
Python, Ruby, etc. The Qt toolkit will be very useful to you in most of
the KDE SC codebase.


Q. What programming concepts do I need to know?

A. An understanding of object-oriented programming (OOP) is valuable if
you want to hack on the KDE development platform, but is not necessary
for developing applications. You need also to know about software
version control and the Qt communications concepts of signals and
slots.


Q. How do I read backtraces?

A. This is covered in the *Reading Backtraces* section of this book.


Q. How do I use Valgrind?

A. Please refer to
http://techbase.kde.org/Development/Tools/Valgrind for a brief overview
of Valgrind.


Q. What's the best distro for KDE SC development?

A. Any distribution that provides the latest and greatest KDE SC
packages is just fine for developing the KDE SC.


Q. How do I report a bug?

A. Use Bugzilla, our bug tracking software. A good bug report is
thorough (containing all details that could be relevant, such as the
operating system you're using, the versions of all related software, and
the precise actions you were doing when the bug occurred). It should
also be factual, polite, and clearly written. For a quick introduction
to bugzilla, please refer to:

   http://techbase.kde.org/Contribute/Bugsquad/Quick_Introduction_to_Bugzilla


For more Frequently Asked Questions visit
http://techbase.kde.org/Category:FAQs
