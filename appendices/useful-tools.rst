Useful Tools
============

This chapter lists some of our favorite tools for software development,
debugging. building, and other activities. Some are discussed in other
parts of the book as well.

Core Tools
----------

These are absolutely required to build and develop KDE software. On
Linux, they will typically be provided by your distribution. On other
platforms, packages should normally be available, often for download
directly from the home page of the tool. Of course, the standard
development tools, such as a C/C++ compiler and some sort of text
editor, are also required.

**CMake** - CMake is the build system of choice for the KDE SC. Once you
have this, you can use it to configure a software project for building,
and that process will tell you of any other requirements you are
missing.

**Git** - Most KDE SC projects are developed in Git, so you will need it
to get the source code in the first place. You can find the relevant Git
URLs at the KDE SC projects directory. It will be helpful to follow the
git configuration instructions.

**Subversion** - The KDE SC still uses Subversion for some things,
notably translations.

Debugging and Analysis Tools
----------------------------

These are used to analyze a program and do such tasks as profiling.

**Valgrind** - Valgrind helps to find memory leaks and uninitialized
memory blocks. It also includes a profiler and more. Valgrind is one of
the most important development tools.

**The GNU Project Debugger (GDB)** - GDB helps you find problems in
source code by allowingyou to set breakpoints, step through the code,
look at stack traces, etc. It keeps evolving, but we currently recommend
version 6.x. Graphical frontends are available. See also the debugging
tutorial "Debugging with GDB"
at http://techbase.kde.org/Development/Tutorials/Debugging/Debugging_with_GDB

**KDbg and DDD** - KDbg and DDD are graphical user interfaces to GDB.

**MS Windows tools (Process Explorer, Console, WinDbg, DebugView,
etc.)** - More information about these is available on the "KDE on
Windows" page on Techbase:

   http://techbase.kde.org/Projects/KDE_on_Windows

Development Tools
-----------------

These are more basic tools that will help you with your development. We
have discussed some of these earlier.

Qt Creator
~~~~~~~~~~

This is the recommended IDE for KDE SC development, making it easy to
write new programs and to integrate existing ones into an IDE. Further
details can be found in this tutorial:

   `http://www.developer.nokia.com/Community/Wiki/How_to_use_Qt_Creator_IDE <http://www.developer.nokia.com/Community/Wiki/How_to_use_Qt_Creator_IDE%20>`__

KDevelop
~~~~~~~~

KDevelop is the IDE created as part of the KDE SC for developing KDE and
Qt C++ applications. It includes a an integrated debugger, a powerful
editor with syntax highlighting, a Project wizard to create applications
from templates, the automake/autoconf suite, and even the class
documentation. Further details can also be found on the KDevelop wiki.

http://kdevelop.org/

Eclipse
~~~~~~~

Eclipse was developed for Java programming, but designed with a view
toward extensibility that has made it popular for many programming
languages. You can find out more
at `http://www.eclipse.org <http://www.eclipse.org/>`__.

MS Visual Studio® Express IDE (Windows only)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Visual C++® Express is the free version of the Microsoft Visual Studio
compiler from, and is officially supported by Nokia. It is a choice for
compiling Qt and KDE applications on Windows. More information can be
found at:

   http://techbase.kde.org/Projects/KDE_on_Windows

Internationalization (i18n) Tools
---------------------------------

These help you create applications that can be viewed and used in
different languages and with the local conventions of different
countries for currency, time, etc.

Lokalize
~~~~~~~~

Lokalize is a computer-aided translation (CAT) system that focuses on
productivity and quality assurance. It has the usual components for CAT
tools: translation memory, a glossary, etc. It also includes a unique
translation merging (synchronization) capability. It is targeted at
software translation and integrates external conversion tools for
freelance office document translation.

Dr. Klash
~~~~~~~~~

This reports conflicting shortcuts in menus. It's helpful for both
translators and developers.

**The x-test language**

This language helps you find untranslated strings in applications. If
you start your application with the "x-test" locale, all translated
strings will appear with trailing and leading xx's.

Helper Tools
------------

These provide a variety of useful functions.

**kde4-config**

This helps a user to find out more about a KDE SC installation.

**kfmclient**

This lets you control the Konqueror browser through scripts.

**kioclient**

Command-line tool for network-transparent operations

**kconf_update**

This updates configuration files.

**apidox tools**

These help develop and generate API documentation for your code.

**Automoc4**

This is a helper tool that builds KDE SC4. It automatically generates
moc-files.

**svnmerge.py**

This tool helps you keep track of merges between different SVN branches.

Quality Assurance
-----------------

Code Review
~~~~~~~~~~~

KDE uses the Review Board for performing code reviews; see the page on
the Review Board on the kDE Techbase for more information:

   http://techbase.kde.org/Development/Review_Board

Continuous Building - Dashboards
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dirk's dashboard has all KDE modules, while the win32 dashboard has a
selection available on Windows.

English Breakfast Network - Static Analysis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The English Breakfast Network is a collection of machines that do
automated KDE source artifact quality checking. Basically, that means
they have a git clone of the entire KDE codebase (including
documentation and whatnot) and they run checking tools on that.

EBN uses:

-  Krazy - Code Analysis
-  APIDOX - API Documentation
-  Sanitizer - DocBook Checker
-  Usability Checks
