About this book
===============

Acknowledgments
~~~~~~~~~~~~~~~

A few people have really helped us make this book what it is so this
book is incomplete without a vote of thanks and a hug of appreciation to
them. So , in no specific order:

-  Gunner, from Aspiration (http://www.aspirationtech.org/),
   inspired and made us all smarter, as did the other teams at the
   sprint, namely OpenMRS, OpenStreetMap, and Sahana Eden.

-  Nóirín Plunkett & Belinda Lopez provided amazing writing and editing
   resources for all of us.

-  Adam from Floss Manuals was endlessly helpful, and the Booki team
   worked with us on needed functionality and bug-fixing as the week and
   work went along.

We also thank the KDE community, who provided for us both an atmosphere
in which we could grow and thrive as contributors and human beings, but
also for providing wonderful documentation from which we could steal
with wild abandon.

In particular, Ingo Malchow, Lydia Pintscher, and the KDE-Promo team
gave us great help in our time-pinch.

The Team
~~~~~~~~

This book was first created at a three-day book sprint in October,
2011, at the Googleplex in Mountain View, California. Carol Smith of
Google Summer of Code fame got us the needed funding to fly Karan,
Supreet, and Rohan from India, and to house and feed us while we
worked. Rohan turned 21 during the sprint, and the group celebrated
with an X-box party!

- Rohan Garg - rohan16garg@gmail.com
- Supreet Pal Singh - supreetpal@gmail.com
- Karan Pratap Singh - wizard.karan@gmail.com
- Valorie Zimmerman - valorie.zimmerman@gmail.com
- Andy Oram - andyo@oreilly.com

.. image:: /images/IMG_0153_1.JPG

*KDE team at the Doc Sprint, October 2011*
