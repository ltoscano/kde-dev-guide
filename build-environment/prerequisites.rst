Prerequisites
=============

Before you actually build the KDE development environment from source,
think -- do you need to do this? Yes, if you will be working on the KDE
SC core. You will not need the entire environment built from source if
you intend to work on one application. We recommend confirming whether
you need to build the environment by posting a question on the mailing
list or chatting up on the IRC.

Assuming you need to build KDE from source, you will need to set up your
build environment and install packages that contain header files used
during the compilation of the KDE platform sources.

Setting up the Environment
--------------------------

In order to set up the KDE platform build environment, you need to
create a file defining environment variables and other settings by
following the instructions at:

http://techbase.kde.org/Getting_Started/Build/Environment#Environment_Configuration

Save the file in the root directory for your build environment with the
name .build-config. If you need separate build environments--such as one
for stable releases and one for nightly builds--it is recommended that
you create a separate script for each build environment and leave it in
the environment's root directory.

Build Requirements
------------------

This section details the software requirements you must install on your
system before you can start building the KDE platform. For most of these
requirements, it is best to use the packages provided by your operating
system distribution, but in some cases you will need to build these
requirements yourself.

Debian/Ubuntu
~~~~~~~~~~~~~

Minimum build dependencies on Debian/Ubuntu for KDE 4.6 and above are
as follows:

.. code-block:: console

   sudo apt-get install graphviz libxml2-utils libopenexr-dev libjasper-dev libenchant-dev \
   libavahi-common-dev libaspell-dev libasound2-dev libldap2-dev libsasl2-dev \
   libsmbclient-dev libxkbfile-dev libxcb1-dev libxklavier-dev libxdamage-dev \
   libxcomposite-dev libbluetooth-dev libusb-dev network-manager-dev \
   libsensors4-dev libnm-util-dev libcfitsio3-dev libnova-dev libeigen2-dev \
   libopenbabel-dev libfacile-ocaml-dev libboost-python-dev libsvn-dev libsvncpp-dev \
   libqt4-dev libqca2-dev libstreamanalyzer-dev libstrigiqtdbusclient-dev \
   libcommoncpp2-dev libidn11 libidn11-dev libpci-dev libxss-dev libxft-dev \
   libpolkit-agent-1-dev libpolkit-backend-1-dev libpolkit-gobject-1-dev git libpoppler-qt4-dev \
   libspectre-dev

Extra and optional packages for Debian/Ubuntu can be found at:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   http://techbase.kde.org/Getting_Started/Build/Distributions/Debian

openSUSE
~~~~~~~~

Minimum build depends for openSUSE are as follows:

.. code-block:: console

    yast -i alsa-devel automoc4 avahi-devel patch cups-devel kde4-filesystem \
    libbz2-devel avahi-compat-mDNSResponder-devel hal-devel xorg-x11-devel \
    libQtWebKit-devel libxml2-devel kdesdk4 clucene-core-devel boost-devel \
    libjpeg-devel liblrdf-devel libpng-devel libxslt-devel libredland-devel \
    Mesa-devel giflib-devel subversion gcc-c++ gmp-devel xine-devel \
    libgpgme-devel pcre-devel dbus-1-devel libqt4-devel cmake git \
    doxygen polkit-devel docbook-xsl-stylesheets cyrus-sasl-devel libical-devel

Extra and optional packages for openSUSE can be found at:

   http://techbase.kde.org/Getting_Started/Build/Distributions/openSUSE

Other distributions
~~~~~~~~~~~~~~~~~~~

Please refer
to http://techbase.kde.org/Getting_Started/Build/Distributions
