Scripted KDE Builds (kdesrc-build)
==================================

The easiest way to build the KDE Software Compilation (SC) from scratch
is to run a script named kdesrc-build (formerly kdesvn-build), written
by Michael Pyne. This approach is highly recommended for those new to
building the KDE SC, because it takes care of almost the whole process
for you. The builds remain compatible with the manual methods of
building KDE SC, so you can change the modules you install later if
you want.

*kdesrc-build* automates the following tasks and more:

-  Performing the initial checkout
-  Handling updates for modules that are already checked out
-  Setting up the build system for the module
-  Performing the build and install
-  Specifying your CMake options or configure flags (so you don't have
   to remember them every time)
-  Logging build errors so you can review them more easily for
   troubleshooting

This is not the be all and end all for your troubles building the KDE
SC. Troubleshooting may still be required. Many errors that occur using
other methods occur here too: you should read the log files that are
stored for you.

Useful links for finding modules
--------------------------------

When using *kdesrc-build*, you may find it beneficial to search the
source repository for modules. Sources include the following:

-  To browse any of the various KDE SC projects using git, you can go
   to https://projects.kde.org/  or
   to http://gitweb.kde.org/.
-  To browse the KDE Subversion repository,
   use http://websvn.kde.org/trunk/KDE/

Setup
-----

These subsections cover all the things you need to get and
install kdesrc-build itself.

Prerequisites
~~~~~~~~~~~~~

*kdesrc-build* is fairly easy to install and set up, but you need to
have the right software installed to build KDE SC. The requirements to
build KDE SC are available at:

   http://techbase.kde.org/Getting_Started/Build/Requirements

*kdesrc-build* requires Perl 5.8 or higher. It is installed by default
with most distributions, and is included in the previous link. Check
your version of Perl with:

.. code-block:: console

   perl -v

You will also need libwww , a collection of Perl Internet-related
modules from `https://github.com/gisle/libwww-perl <https://github.com/gisle/libwww-perl#readme>`__.

Downloading and installing kdesrc-build
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once your system is set up and able to compile the KDE SC, you can
download *kdesrc-build* from its website, http://kdesrc-build.kde.org .
The file you download will contain (at least) the *kdesrc-build* script
and a sample configuration file.ut Installing *kdesrc-build* is as
simple as saving the file and making it executable. If you'd like, you
can move it to a directory in your PATH, however for this example we'll
put it into the KDE SC source directory that we use (*~/kdesrc*).

.. code-block:: console

   mkdir -p ~/kdesrc &&
   cd ~/kdesrc &&
   tar xjvf ~/path/to/kdesrc-build-1.12.tar.bz2 &&
   cp kdesrc-build-1.12/kdesrc-build .

Alternatively, the newest kdesrc-build script (and sample config file)
can be pulled down directly using git:

.. code-block:: console

   git clone git://anongit.kde.org/kdesrc-build.git ~/kdesrc

Setting up the configuration
----------------------------

Configuration options for *kdesrc-build* are taken from a file named
*~/.kdesrc-buildrc*. Directions for editing this file are at:

   http://techbase.kde.org/Getting_Started/Build/kdesrc-buildrc

For the most part the defaults in the included kdesrc-buildrc-sample should
be sufficient. You can copy it to your home directory as follows:

.. code-block:: console

   cp ~/kdesrc/kdesrc-build-1.12/kdesrc-buildrc-sample ~/.kdesrc-buildrc
   # Now edit the ~/.kdesrc-buildrc


Note that the config file name begins with a leading dot (.), making it
a hidden file. If you are using Dolphin or Konqueror from the desktop,
you may need to show hidden files in order to find the configuration
file and edit it. Alternatively, you can edit the sample file before
copying it to ~/.kdesrc-buildrc.

Also, make sure that the modules you'll want to build are included.
You'll want the following at the least:

-  qt-copy
-  kdesupport
-  kdelibs
-  kdepimlibs
-  kdebase

Modules are built in the order they appear in your
*~/.kdesrc-buildrc* file, so the first module should be qt-copy.
kdesupport should be before kdelibs, which should be before any other
KDE SC module, and so on.

The sample configuration file does include these modules by default. So
you won't need to make many changes unless you'd like to add some
modules to the build by uncommenting them.

If a module you'd like to build isn't already present, simply add the
following to the end of the ~/.kdesrc-buildrc:

::

   module module-name
   end module

module-name is whatever the module is called in the software repository
(for instance, kdemultimedia).

You may want to enable the make-install-prefix option if you are
installing KDE SC or Qt to a directory that is not in your home
directory. Through make-install-prefix, you can
run *su* or *sudo* during the make install process so you can install
files as root, or set certain programs to execute with higher
permissions. (This is required for certain programs to execute
properly.)

::

   module kdelibs
     make-install-prefix sudo -S # sudo with no stdin
   end module

   module kdebase
     make-install-prefix sudo -S
   end module

Git-based modules
~~~~~~~~~~~~~~~~~

Most of the KDE SC modules are in git, although a few of them still need
to be ported from svn to git. Eventually, all KDE SC modules and
projects will be in git.

To build these modules in *kdesrc-build*, you just need to add a couple
of lines to the module configuration. For example, konversation is
developed in the Git repository at:

   https://projects.kde.org/projects/extragear/network/konversation/repository

So to add this module, write the following at the end
of *~/.kdesrc-buildrc*:

::

   module konversation
       repository git://anongit.kde.org/konversation
       branch master
   end module

In this case I selected the "master" branch since that is the default
git branch.

Now whenever you build konversation, *kdesrc-build* will use git instead
of Subversion.

Useful kdesrc-build commands
----------------------------

*kdesrc-build* is driven from the command line, so here's a guide to
some of the more useful command line options:

+-----------------------------------+-----------------------------------+
| Option                            | Effect                            |
+===================================+===================================+
| --pretend (``-p``)                | This option is like a dry run.    |
|                                   | *kdesrc-build* will process the   |
|                                   | options and its configuration     |
|                                   | like normal, and run through the  |
|                                   | build as normal, but instead of   |
|                                   | downloading or running the build  |
|                                   | it will just print messages about |
|                                   | what it would have done. You      |
|                                   | should always run with -p before  |
|                                   | running the script for real, to   |
|                                   | make sure it is doing what you    |
|                                   | expect.                           |
+-----------------------------------+-----------------------------------+
| --nosvn (``--no-src``)            | This option skips the source code |
|                                   | update step. This is useful if    |
|                                   | you're running *kdesrc-build*     |
|                                   | again soon after a previous       |
|                                   | update and don't want to wait     |
|                                   | just to find out there were no    |
|                                   | changes.                          |
+-----------------------------------+-----------------------------------+
| ``--refresh-build``               | This option causes *kdesrc-build* |
|                                   | to delete the current build       |
|                                   | information for the modules given |
|                                   | on the command line and build     |
|                                   | them again from scratch. This     |
|                                   | option adds a lot of time but     |
|                                   | offers the best chance of a       |
|                                   | successful build.                 |
+-----------------------------------+-----------------------------------+

Any non-option arguments on the command line are assumed to be modules
to build (and are built in the order provided on the command line). If
no modules are specified, all of the modules listed in the
~/.kdesrc-buildrc are built, in the order listed in the file.

Building the KDE SC
-------------------

We're almost there. If you're happy with your settings, it's time to
test out *kdesrc-build*. In theory things are as simple as running
kdesrc-build and then coming back later.

.. code-block:: console

   cd ~/kdesrc
   ./kdesrc-build


You may want to test it by building just qt-copy first, however.

.. code-block:: console

   cd ~/kdesrc
   ./kdesrc-build qt-copy

If the build failed (*kdesrc-build* will error out with a nice bright
red error message), there are several possible things to check for:

#. You may be missing a key piece of required software (such as a
   development library)
#. The KDE SC code being compiled may be broken in some fashion so it
   won't build. This is commonly due to newly committed code that worked
   on the developer's machine, or occasionally because it is Monday
   (when incompatible changes are permitted to kdelibs).
#. *~/.kdesrc-buildrc* may not be set up properly. You may be trying to
   install to a directory that you do not have permission to access, for
   instance, or you may have specified a system qtdir that does not
   exist.
#. The module may depend on a newer version of qt-copy or kdelibs (or
   another module). In this case you'll have to run *kdesrc-build* to
   update the out-of-date module first.

| 
| 

How do you find out what the error was? The output of the failing
command will be in the log directory. By default, all log output is in
the *log* subdirectory of the KDE SC source directory. The log
directory is laid out like
this: *log/date-run/module/output-file.log*. To simplify finding the
appropriate file, a couple of symlinks are created:

log/latest

   Contains the debugging output from the last time *kdesrc-build* ran
   (--pretend does not affect this).

log/latest/*module*/error.log

   Contains the debugging output for the command that caused the build
   of the specified *module* to fail.

For instance, if qt-copy just failed to build, you could read the output
like this:

.. code-block:: console

   cd ~/kdesrc
   kwrite log/latest/qt-copy/error.log

Replace kwrite with your preferred editor. Hopefully the output can
guide you to resolving the problem. For instance, if the failure is
CMake output saying you're missing a library, install that library and
try again. For link errors, you can try running with the--refresh-build
on the module (or if that doesn't work, on required libraries like
qt-copy and kdelibs).

If you're stumped by the error, you may want to wait a day and try
updating again, and hope that the reason for the error has been fixed.
You can also try mailing the kde-devel mailing list to see whether
others know about the problem or have had similar issues.

Running your new KDE installation
---------------------------------

Assuming you got enough of the modules to build and install to have a
working KDE SC installation, you'll still need to set up your
environment correctly to run it. *kdesrc-build* doesn't help you out
here (yet), so you should follow the instructions at:

http://techbase.kde.org/Getting_Started/Using_an_IDE_with_KDE4

Make sure to use the same paths as the ones you defined in
*~/.kdesrc-buildrc*. For the KDEDIRS and KDEDIR variables, use the
setting of the "prefix" option (in the global section). For the QTDIR
variable, use the setting of the "qtdir" option.

Keeping your KDE installation up to date
----------------------------------------

Keeping your KDE installation up to date is as simple as running
*kdesrc-build* again. Every build has these phases:

#. Update the source code for all modules being built.
#. Build and then install all the modules.

Old build directories are not deleted by default, so the build after a
small update will not normally take as long as the initial build of a
module. This is called "incremental make". However it may be necessary
at times to perform a full rebuild, due to inconsistencies between the
build directory configuration and changes to the source directory. You
can use the--refresh-build option to force a full rebuild.

For more information on how to take advantage of kdesrc-build, see the
documentation at:

   http://kdesrc-build.kde.org/documentation/

The site describes all of the module options and command line options
available for *kdesrc-build* and gives tips on how to perform various
useful tasks.
