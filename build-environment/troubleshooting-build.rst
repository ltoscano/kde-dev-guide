Troubleshooting your KDE Build
==============================

Compile and Linking errors are frequent sources of discouragement. Make
careful note of the first occurrence of an error in your build process.
It could be as simple as a bad environment variable, an unexpected
version of a library or missing prerequisite. Please read the
instructions carefully. Check for spelling errors while using module
names as well as commands.

Please review your logs and do searches for fixes. If you cannot find a
solution then please ask for help on IRC or a Mailing List.
