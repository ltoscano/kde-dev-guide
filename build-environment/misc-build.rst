Other Ways to Build the KDE SC
==============================

You don't always have to build the KDE SC from bare sources. Many
pre-packaged builds of the KDE SC are available. This chapter will focus
on installing KDE SC from the git master without compiling it from
sources.

Project Neon
------------

The wonderful Project Neon team provides daily builds of the KDE SC for
developers using Kubuntu. Project Neon installs binaries and libraries
in */opt/project-neon/*, which gives you a sandboxed installation of the
master KDE SC alongside your stable KDE SC installation.

Get Project Neon from its Launchpad home page:
https://launchpad.net/~neon. You can install the packages using the
following command:

.. code-block:: console

   sudo add-apt-repository ppa:neon/ppa && sudo apt-get update && sudo apt-get install project-neon-base

More information on Project Neon can be found at:

   https://wiki.ubuntu.com/Kubuntu/ProjectNeon

   http://techbase.kde.org/Getting_Started/Using_Project_Neon_to_contribute_to_KDE

openSUSE
--------

This GNU/Linux distribution provides frequent snapshots of the KDE SC
master. The target is to provide snapshots on a weekly basis, but this
is not always possible. These packages may not always include openSUSE
specific patches, and are not available for the oldest openSUSE
releases. You must add the core packages repository to use Extra or
Unstable:Playground.

**Version: 11.4**

**Core packages:**
http://download.opensuse.org/repositories/KDE:/Unstable:/SC/openSUSE_11.4/

**Released applications:**
http://download.opensuse.org/repositories/KDE:/UpdatedApps/openSUSE_11.4/

**Extra:**
http://download.opensuse.org/repositories/KDE:/Extra/openSUSE_11.4_KDE_Unstable_SC/

**Unstable:Playground:**
http://download.opensuse.org/repositories/KDE:/Unstable:/Playground/openSUSE_11.4_KDE_Unstable_SC/


**Version: 11.3**

**Core packages:**
http://download.opensuse.org/repositories/KDE:/Unstable:/SC/openSUSE_11.3/

**Released applications:**
http://download.opensuse.org/repositories/KDE:/UpdatedApps/openSUSE_11.3/

**Extra:**
http://download.opensuse.org/repositories/KDE:/Extra/openSUSE_11.3_KDE_Unstable_SC/

**Unstable:Playground:**
http://download.opensuse.org/repositories/KDE:/Unstable:/Playground/openSUSE_11.3_KDE_Unstable_SC/

**Version: Factory**

**Core packages:**
http://download.opensuse.org/repositories/KDE:/Unstable:/SC/openSUSE_Factory/

**Released applications:** Use the application packages from
openSUSE:Factory

**Extra:**
http://download.opensuse.org/repositories/KDE:/Extra/openSUSE_Factory_KDE_Unstable_SC/

**Unstable:Playground:**
http://download.opensuse.org/repositories/KDE:/Unstable:/Playground/openSUSE_Factory_KDE_Unstable_SC/

You can find more information about openSUSE's master KDE builds at:

   http://en.opensuse.org/KDE_repositories#Unstable:SC_aka._KUSC_.28KDE_trunk.29
