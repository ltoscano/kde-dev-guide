=================================
KDE development build environment
=================================

.. toctree::
   :maxdepth: 2

   prerequisites
   misc-build
   kdesrc-build
   troubleshooting-build
